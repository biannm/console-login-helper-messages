%global github_owner    coreos
%global github_project  console-login-helper-messages

Name:           console-login-helper-messages
Version:        0.21.3
Release:        1
Summary:        Combines motd, issue, profile features to show system information to the user before/on login
License:        BSD
URL:            https://github.com/%{github_owner}/%{github_project}
Source0:        https://github.com/%{github_owner}/%{github_project}/archive/v%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  systemd make
%{?systemd_requires}
Requires:       bash systemd

%description
%{summary}.

%package motdgen
Summary:        Message of the day generator script showing system information
Requires:       console-login-helper-messages
Recommends:     openssh
Requires:       bash systemd
Requires:       setup >= 2.12.7-1
Requires:       ((pam >= 1.3.1-15) if openssh)
Requires:       selinux-policy >= 3.14.2-50
Requires:       util-linux >= 2.36-1

%description motdgen
%{summary}.

%package issuegen
Summary:        Issue generator scripts showing SSH keys and IP address
Requires:       console-login-helper-messages
Requires:       bash systemd setup
Requires:       (NetworkManager)
Requires:       openEuler-release
Requires:       util-linux >= 2.36-1

%description issuegen
%{summary}.

%package profile
Summary:        Profile script showing systemd failed units
Requires:       console-login-helper-messages
Requires:       bash systemd setup

%description profile
%{summary}.

%prep
%autosetup -p1

%build

%install
make install DESTDIR=%{buildroot}

%post issuegen
%systemd_post %{name}-gensnippet-ssh-keys.service

%preun issuegen
%systemd_preun %{name}-gensnippet-ssh-keys.service

%postun issuegen
%systemd_postun_with_restart %{name}-gensnippet-ssh-keys.service

%post motdgen
%systemd_post %{name}-gensnippet-os-release.service

%preun motdgen
%systemd_preun %{name}-gensnippet-os-release.service

%postun motdgen
%systemd_postun_with_restart %{name}-gensnippet-os-release.service

%files
%doc README.md
%doc doc/manual.md
%license LICENSE
%dir %{_libexecdir}/%{name}
%dir %{_prefix}/lib/%{name}
%dir %{_prefix}/share/%{name}
%{_prefix}/lib/%{name}/libutil.sh
%{_tmpfilesdir}/%{name}.conf

%files issuegen
%{_unitdir}/%{name}-gensnippet-ssh-keys.service
%{_sysconfdir}/NetworkManager/dispatcher.d/90-%{name}-gensnippet_if
%{_prefix}/lib/%{name}/issue.defs
%{_tmpfilesdir}/%{name}-issuegen.conf
%{_libexecdir}/%{name}/gensnippet_ssh_keys
%{_libexecdir}/%{name}/gensnippet_if
%{_libexecdir}/%{name}/gensnippet_if_udev

%files motdgen
%{_unitdir}/%{name}-gensnippet-os-release.service
%{_prefix}/lib/%{name}/motd.defs
%{_tmpfilesdir}/%{name}-motdgen.conf
%{_libexecdir}/%{name}/gensnippet_os_release

%files profile
%{_prefix}/share/%{name}/profile.sh
%{_tmpfilesdir}/%{name}-profile.conf
%ghost %{_sysconfdir}/profile.d/%{name}-profile.sh

%changelog
* Thu Feb 2 2023 biannm <bian_naimeng@hoperun.com> - 0.21.3-1
- Upgrade to 0.21.3

* Mon Jan 24 2022 fushanqing <fushanqing@kylinos.cn> - 0.21.2-1
- Initial package
